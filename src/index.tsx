import * as React from "react";
import { css } from "@emotion/core";

const baseStyle = css`
  border: 1px solid red;
`;

export default class Component extends React.Component {
  render() {
    return <div css={baseStyle}>Hello</div>;
  }
}
