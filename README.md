## Bundling Emotion

Minimal working example meant to look into why I couldn't get Emotion to be properly "exported" as part of an ES module.

---

## Getting Started

```
git clone
cd bundling-emotion
yarn
yarn build

# Use Storybook to see the problem
yarn storybook

# There's also a watch script for building
yarn watch
```
