const path = require('path');
const webpack = require('webpack');

const BABEL_TYPESCRIPT_OPTIONS = {
  presets: [
    ['@babel/preset-env', { useBuiltIns: 'entry', corejs: 3 }],
    '@babel/preset-typescript',
    '@babel/preset-react',
    '@emotion/babel-preset-css-prop',
  ],
  plugins: [
    '@babel/plugin-proposal-class-properties',
  ],
};

module.exports = async ({ config }) => {
  config.resolve = config.resolve || {};
  config.resolve.extensions.push('.tsx', '.ts');

  config.plugins.push(new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/));
  // Avoid parsing large libraries to speed up build
  config.module.noParse = /jquery|moment/;

  // To enable live debugging of other packages when referring to `src`
  config.module.rules.push({
    include: path.resolve(__dirname, '../src'),
    exclude: /node_modules/,
    test: /\.tsx?$/,
    use: config.module.rules[0].use,
  });

  // Enable TypeScript
  config.module.rules.push({
    include: path.resolve(__dirname, '../src'),
    exclude: /node_modules/,
    test: /\.(ts|tsx)$/,
    use: [
      {
        loader: 'babel-loader',
        options: BABEL_TYPESCRIPT_OPTIONS,
      },
    ],
  });

  config.module.rules.push({
    include: path.resolve(__dirname, '../stories'),
    exclude: /node_modules/,
    test: /\.(ts|tsx)$/,
    use: [
      {
        loader: 'babel-loader',
        options: BABEL_TYPESCRIPT_OPTIONS,
      },
    ],
  });

  config.optimization = config.optimization || {};
  config.optimization.splitChunks = {
    chunks: 'async',
  };

  if (process.env.RUNNING_CONTEXT === 'netlify') {
    config.devtool = false;
    config.cache = false;
  }

  return config;
};
