import { storiesOf, setAddon } from '@storybook/react';
import JSXAddon from 'storybook-addon-jsx';

setAddon(JSXAddon);

const EMPTY_EXAMPLES = [
  {
    renderStory: () => 'Does your default export have an `examples` key?',
    storyName: 'No examples found',
  },
];

const requireContext = require.context('./', true, /index\.jsx?$/);

requireContext.keys().forEach(story => {
  const storyExport = requireContext(story);

  if (storyExport && storyExport.default && !Array.isArray(storyExport.default)) {
    const { examples = EMPTY_EXAMPLES } = storyExport.default;

    examples.forEach(example => {
      const {
        storyPath = 'Missing story path',
        storyName = 'Missing name',
        renderStory = () => 'Missing `renderStory`',
        options = {},
      } = example;

      storiesOf(storyPath, module)
        .addParameters({ options })
        .addWithJSX(storyName, renderStory);
    });
  }
});
