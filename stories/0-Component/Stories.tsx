import * as React from "react";
import Component from "../../src";
import { default as C } from "../../dist/index.es.js";

export default [
  {
    storyName: "Right from source",
    storyPath: "Component|Component",
    renderStory: () => <Component />
  },
  {
    storyName: "ES Module",
    storyPath: "Component|Component",
    renderStory: () => <C />
  }
];
